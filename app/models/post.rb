class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	validates_presence_of :titile
	validates_presence_of :body
end
